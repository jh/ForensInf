---
author: "Jack Henschel"
title: "Forensischer Bericht zur Asservatennummer 34-787"
header: "Forensischer Bericht Asservatennr. 34-787"
date: 2018-06-05
lang: de-DE
papersize: a4
documentclass: article
fontsize: 12pt
toc: true
titlepage: true
toc-own-page: true
---

## Prolog

### Beweismittelkette

Das Asservat 34-787 wurde dem Gutachter per Kurier am 18.05.2018 übermittelt.

Hierbei handelt es sich um eine externe Festplatte auf der das Abbild der Festplatte eines Dell XPS Laptops gespeichert ist. Unmittelbar nach Erhalt des Asservat wurde die SHA-256-Checksumme der Datei gebildet.

```
2d087c1c86ad6d3589b26d315d8dd7956dd5dbb27dc880418ef05c40b845f5b0  exercise_2.img
```

Diese stimmt mit der von der Staatsanwaltschaft übermittelten Prüfsumme überein. Diese wurde während der Beschlagnahmung des Rechners durch einen Hardware-Write-Blocker der Marke WiebkeTech SAS angefertigt. Dies ist dem Sicherungsbericht zum Festplattenabbild der Kriminalinspektion 5 zu entnehmen.

Der Originaldatenträger wird weiterhin in der Asservatenkammer des Polizeipräsidiums verwahrt. Am Ende der Untersuchungen des Gutachters wird die Checksumme erneut überprüft, um die Integrität des Abbilds zu gewährleisten.

### Arbeitsumgebung

Bei dem Rechner auf dem die Untersuchungen durchgeführt werden handelt es sich um ein speziell angefertigtes x86-System, bestehend aus einem Intel i5-2500K Prozessor, 8GB DDR3 Arbeitsspeicher, einer MSI Z77A-GD55 Hauptplatine sowie einer 120GB A-DATA SSD als nichtflüchtigen Datenspeicher.

Auf dem Rechner selbst läuft ein Debian Stretch GNU/Linux System (9.4), worauf die Tools "The Sleuth Kit" in Version 4.4.0 und "TestDisk" in Version 7.0 installiert sind.
Des weiteren wird das Hexdump-Programm `hd` aus der bsdmainutils Sammlung 9.0.12, `sqlite3` Version 3.16.2, `grep` Version 2.27 sowie `file` Version 5.30 verwendet.

\newpage

### Auftrag

Der Untersuchungsauftrag besteht aus folgenden Fragen:

1. Gibt es im Browserverlauf im Zeitraum vom 1. Dezember 2016 bis 24. März 2017 auffällige Suchanfragen die in Verbindung mit der Tat stehen könnten?

2. Befinden sich auf der Festplatte Fotos vom Tatopfer? Befinden sich auf der Festplatte Dateien, die auf eine systematische Überwachung und Dokumentation der Aufenthaltsorte des Tatopfers hindeuten?

3. Gab es am 24. März 2017, dem Tag der Tatbegehung, zwischen 18:00 Uhr und 22:00 Uhr ein Skype Gespräch zwischen Kurt Kunz und Veronika Vogel beziehungsweise gibt es Spuren, die daraufhin deuten?

4. Ist ein offline Zugriff auf das E-Mail Konto des Besitzers möglich und falls ja, finden sich darin möglicherweise auffällige E-Mails im Zeitraum vom 1. Februar bis 20. Mai 2017?

5. Existieren auf der Festplatte zwischengespeicherte (Cache) oder temporäre Daten, die daraufhin deuten, dass Kurt Kunz im Zeitraum vom 1. Dezember 2016 bis 24. März 2017 über das Internet einen Zimmermannhammer der Marke Nagelflix beziehungsweise einen Werkzeugkoffer derselben Marke erworben hat?

6. Lassen sich gelöschte Dokumente aus dem Zeitraum 1. Dezember 2016 bis 24. Mai 2017 wiederherstellen, die möglicherweise Hinweise auf die Tat geben?

7. War der Laptop zum Tatzeitpunkt, dem 24. März 2017, in Betrieb und bestand eine Internetverbindung?

Die Staatsanwaltschaft Nürnberg-Fürth erwartet den Bericht bis 5. Juni 2018.

\newpage

## Zusammenfassung

Im Browserverlauf finden sich keine auffälligen Suchanfragen, die auf eine Verbinding zur Tatbegehung hindeuten könnten (siehe [Browserverlauf](#browserverlauf)).

Bei der Untersuchung des Asservats wurden keine Dokumente, Fotos oder ähnliches gefunden, die darauf hindeuten, dass Kurt K. das Opfer systematisch überwacht hat oder die Tat genau geplant hat (siehe [Fotos und Überwachung des Tatopfers](#fotos-und-uxfcberwachung-des-tatopfers)).

Laut Skype-Protokolldateien hat Kurt K. mit Veronika V. zum ersten Mal am 25. März gegen 20:20 geschrieben (siehe [Skype Nutzung](#skype-nutzung)).
Jedoch ist auch ein Gespräch zwischen den beiden am 24. März um 18:23 (Dauer: 49 Minuten) protokolliert.
Diese Inkonsistenz ist auch bei der Untersuchung der Systemprotkolldateien ersichtlich geworden (siehe [Betrieb des Rechners](#betrieb-des-rechners)).

Am 25. März um 20:47 springt die Systemzeit auf den 24. März 18:23 zurück.
Diese Zeit wird jedoch kurz darauf durch die NTP Dienst, welcher automatisch der lokale Zeit mit einem Server im Netzwerk abgleicht, korrigiert.
Daraufhin stoppt der Nutzer diesen Prozess und setzt die Zeit erneut auf den 24. März 18:23.
In diesem Zustand läuft das System dann etwa eine Stunde (19:15 "falsche Zeit"), dann wird der NTP Dienst wieder gestartet und die Zeit wird auf den 25. März 21:47 korrigiert.

Damit ist die Inkonsistenz in Skype-Protokolldateien zu erklären.
Ein möglicher Grund, warum diese Änderung an der Rechnerzeit vorgenommen wurde, ist um eine Skype Gespräch "zu dem Tatzeitpunkt" zu führen.

Ein Offline-Zugriff auf das E-Mail Postfach von Kurt K. ist nicht möglich.
Jedoch konnte der Ausschnitt einer E-Mail an Veronika V. gesichert werden (siehe [Zugriff auf E-Mail-Postfach](#zugriff-auf-e-mail-postfach)).
Diese war im Zwischenspeichers des Rechners abgelegt worden, als der Beschuldigte Gmail mit seinem Browser geöffnet hat.
Der Klartext dieser Nachricht lautet wie folgt:

> Hallo Schatzi, ich werde den Koffer gleich ins Auto stellen, damit ich ihn morgen nicht vergesse.

> 2017-03-24 17:50 GMT+01:00 Veronika Vogel:
>
> Hey Schatzi, sch.n,dass du es geschafft hast, .skype zu installieren auf deinem neuen Laptop.
>
> Das k.nnen wir ja direkt .mal ausprobieren. Aber du musst mir am Wochenende erkl.ren, wie man das .auf dem Handyinstalliert.
>
> Oh und vergiss nicht, .morgen den Werkzeugkoffer mitzunehmen, dann k.nnen wir den Gartenzaun .reparieren, ich hab schon gelbe Farbe und Pinsel besorgt.

Veronika Vogel hat ihre Nachricht mit an Sicherheit-grenzender Wahrscheinlichkeit am 24. März um 17:50 geschrieben, da dieser Zeitstempel von Googles Server kommt.
Wann Kurt K. jedoch seine Antwort verfasst beziehungsweise geschrieben hat, lässt nicht klären.

In dem gesamten Asservat finden sich keine Hinweise darauf, dass Kurt K. einen Zimmermannhammer oder einen Werkzeugkoffer der Marke Nagelflix erworben hat (siehe [Zimmermannhammer / Werkzeug der Marke Nagelflix](#zimmermannhammer-werkzeugkoffer-der-marke-nagelflix)).

Es ließen sich keine auf dem Asservat gelöschten Dokumente wiederherstellen (siehe [Wiederherstellung gelöschter Dokumente](#wiederherstellung-geluxf6schter-dokumente)).

Am 24. März wurde der Rechner um 17:17 gestartet und zwischen 17:24 und 18:27 ist Nutzeraktivität in den Systemprotokolldateien zu erkennen.
Die Konsistenz der Protokolleinträge ist zu diesem Zeitpunkt noch gegeben.
Auch muss zu diesem Zeitpunkt eine Internverbindung bestanden haben, da Programme aus dem Internet heruntergeladen wurden (siehe [Betrieb des Rechners](#betrieb-des-rechners)).

\newpage

## Technischer Teil

Zunächst wird der Partitionstyp des Festplattenabbilds geprüft. Es handelt sich in diesem Fall um ein DOS-Partitionsschema, wobei im ersten Sektor, dem Master Boot Record (MBR), drei Partitionen gelistet sind.
```
$ mmstat exercise_2.img
dos
```

```
$ mmls exercise_2.img
DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000002047   0000002048   Unallocated
002:  000:000   0000002048   0016777215   0016775168   Linux (0x83)
003:  -------   0016777216   0016779263   0000002048   Unallocated
004:  Meta      0016779262   0018872319   0002093058   DOS Extended (0x05)
005:  Meta      0016779262   0016779262   0000000001   Extended Table (#1)
006:  001:000   0016779264   0018872319   0002093056   Linux Swap/Solaris x86(0x82)
007:  -------   0018872320   0018874367   0000002048   Unallocated
```

Um die einzelnen Bereiche der Partition besser untersuchen zu können, werden diese zunächst aus dem vollständigen Abbild herausgetrennt. Um dabei die Integrität der einzelnen Abschnitte zu gewährleisten, wird direkt danach die SHA-256-Prüfsumme gebildet. Diese wird am Ende der Untersuchung erneut überprüft.

```
$ for i in {0..7}; do
    mmcat exercise_2.img $i > part$i;
    sha256sum part$i;
done

8d76d04401a40f648c1253a29234819f465a6556436c7c8fae0b5486eb9b6b6c  part0
bcd62afdbad5ff3b617a3d14b48baf196beaa8022c73f8a2164a68374ee9c31e  part1
f8b72ae4b289f9e5361028f2f31e2ff348a074178f9994f27265a3c78efaff56  part2
9aa25cc54fb837a197b2b3f602f43c1dffb7193f82522fb1b4aadaadb8d79f62  part3
45b3397d59a7618bd543434d9c9b7e92b52e80e7314b098072b61acd4071fbac  part4
57990bf28dacc4dc5ad367b090c487100e50ce715ee872ce3785cdd8c050ed60  part5
28108cf0c95c895e0aa3916b1bd1ae94ccb550764899a6518915144690fa3a3e  part6
30e14955ebf1352266dc2ff8067e68104607e750abb9d3b36582b8af909fcb58  part7
```

Interessant sind zunächst vor allem die Partitionen `part2` und `part6`. Erstere ist mit Abstand die größte Partition (ca. 8GB), weswegen es sich voraussichtlich um die Hauptpartition des Betriebssystem handelt. Bei Letzterer handelt es sich um die so genannte swap-Partition, einen spezielle Auslagerungsdatei beziehungsweise -bereich, in dem das Betriebssystem Daten zwischenspeichern kann.

Das Dateisystemanalysewerkzeug fsstat, aus der Sleuth Kit Sammlung, zeigt, dass es in der Partition part2 ein Ext4 Dateisystem ist. Ein Blick in das Hauptverzeichnis des Dateisystems (root-Verzeichnis) bestätigt, dass es sich um ein Linux Betriebsystem handelt (zu erkennen an der Verwendung des [Filesystem Hierarchy Standards (FHS)](https://de.wikipedia.org/wiki/Filesystem_Hierarchy_Standard)).

```
$ fsstat part2
FILE SYSTEM INFORMATION
--------------------------------------------
File System Type: Ext4
Volume Name:
Volume ID: 298dc25bdc309cb58c4a627be64bdcc8

Last Written at: 2017-03-25 21:55:48 (CET)
Last Checked at: 2017-03-23 16:26:47 (CET)

Last Mounted at: 2017-03-25 21:55:50 (CET)
Unmounted properly
Last mounted on: /
...
METADATA INFORMATION
--------------------------------------------
Inode Range: 1 - 524289
Root Directory: 2
Free Inodes: 295665
Inode Size: 256
Orphan Inodes: 8902, 8901, 8239, 8238, 8237,
               393487, 393479, 393484, 393481,
               393485, 393483, 393405,
```

Relevant bei der Ausgabe von fsstat ist dabei, dass das Dateisystem zuletzt am 25. März 2017 um 21:55 Uhr beschrieben wurde.

Die Datei `/etc/os-release` zeigt, dass es sich bei dem System um ein Ubuntu 16.04.1 System handelt.

```
NAME="Ubuntu"
VERSION="16.04.1 LTS (Xenial Xerus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 16.04.1 LTS"
VERSION_ID="16.04"
HOME_URL="http://www.ubuntu.com/"
SUPPORT_URL="http://help.ubuntu.com/"
BUG_REPORT_URL="http://bugs.launchpad.net/ubuntu/"
UBUNTU_CODENAME=xenial
```

### Betrieb des Rechners

Der Rechner wurde am 23. März um 22:17 gestartet und um 00:29 des Folgetages wieder heruntergefahren (`/var/log/syslog.2.gz` zu entnehmen).

Am 24. März wurde der Rechner um 17:17 gestartet (siehe `/var/log/syslog.2.gz`). Ab 17:24 ist dabei Nutzeraktivität in den Logdateien zu sehen, da nach dem Skype Paket in den Repositoryquellen gesucht wird (`/var/log/syslog.1`):

```
Mar 24 17:24:12 Schlepptop /usr/lib/snapd/snapd[1419]: api.go:402: DEBUG: use of obsolete "q" parameter: "/v2/snaps?q=skyp"
Mar 24 17:24:12 Schlepptop /usr/lib/snapd/snapd[1419]: api.go:483: jumping to "find" to better support legacy request "/v2/snaps?q=skyp"
```

Somit muss zu diesem Zeitpunkt auch eine Internetverbindung bestanden haben, da die Anfragen an den Server erfolgreich sind. Derartige Aktivität ist bis 18:27 zu erkennen:

```
Mar 24 18:27:18 Schlepptop /usr/lib/snapd/snapd[1419]: daemon.go:181: DEBUG: uid=1000;@ GET /v2/snaps/bluez 132.138us 404
Mar 24 18:27:38 Schlepptop gnome-session[2811]: (gnome-software:3005): Gs-WARNING **: libreoffice-calc.desktop changing management plugin PackageKit->apt
Mar 24 18:27:38 Schlepptop gnome-session[2811]: (gnome-software:3005): Gs-WARNING **: libreoffice-writer.desktop changing management plugin PackageKit->apt
Mar 24 18:27:38 Schlepptop gnome-session[2811]: (gnome-software:3005): Gs-WARNING **: libreoffice-impress.desktop changing management plugin PackageKit->apt
```

Dies wird bestätigt durch die Logeinträge der Paketverwaltung, welche zeigen, dass durch den Nutzer `kurt` um 18:14 das Paket `ubuntu-restricted-extras` explizit installiert wurde (`/var/log/apt/history.log`):

```
Start-Date: 2017-03-24  18:14:08
Commandline: apt-get install ubuntu-restricted-extras
Requested-By: kurt (1000)
```

Eine Installation von zusätzlichen Paket ist in den meisten Fällen nur möglich, wenn eine Internetverbindung besteht, damit die Installationsdateien heruntergeladen werden können.

Nach 18:27 ist zunächst keine weitere Nutzeraktivität erkennbar, erst gegen 02:39 des Folgetages gibt es weitere Systemlogeinträge, die auf Nutzeraktivität hindeuten.

In der Hauptsystemlogdatei `/var/log/syslog` findet sich jedoch eine interne Inkonsistenz. Die Dateien `syslog.1` sowie `syslog.2.gz` sind dabei chronologisch gesehen vor dieser Datei erstellt worden.

Am 25. März um 20:47 wird die Systemzeit auf den 24. März 18:23 geändert. Da die Logeinträge immer nur an die Datei angefügt werden (es findet keine Sortierung statt), ist die Reihenfolge der Einträge immer chronologisch korrekt, wodurch die Zeitumstellung sichtbar wird.

```
Mar 25 20:47:59 Schlepptop gnome-session[2811]: (gnome-software:3005): Gs-WARNING **: Failed to create permission org.freedesktop.packagekit.trigger-offline-update: GDBus.Error:org.freedesktop.PolicyKit1.Error.Failed: Action org.freedesktop.packagekit.trigger-offline-update is not registered
Mar 24 18:23:00 Schlepptop systemd[3513]: Time has been changed
```

Kurz darauf wird die Systemzeit jedoch wieder zuruckgestellt, dies ist wahrscheinlich durch den NTP Dienst geschehen, der automatisch die Systemzeit einem Netzwerkzeitserver angleicht.

```
Mar 24 18:23:00 Schlepptop systemd[1]: apt-daily.timer: Adding 9h 59min 46.458383s random time.
Mar 25 20:50:37 Schlepptop systemd[3513]: Time has been changed
```

Auch der Nutzer merkt dies, woraufhin dieser den NTP Dienst beendet, was an den Eingaben in der root-Shell sowie den Systemlogs zu erkennen ist.

`/root/.bash_history`

```
sudo service ntp stop
cd /etc/init.d
ls
date
```

`/var/log/syslog`

```
Mar 25 20:54:12 Schlepptop systemd-timedated[4061]: Set NTP to disabled
Mar 25 20:54:12 Schlepptop systemd[1]: Stopping Network Time Synchronization
Mar 25 20:54:12 Schlepptop systemd[1]: Stopped Network Time Synchronization.
Mar 24 18:23:00 Schlepptop systemd[3513]: Time has been changed
```

Gleichzeitig ist zu sehen, dass auch die Zeit wieder auf den 24. März um 18:23 geändert wurde.
Diese Inkonsistenz wird auch durch die Kernellogdateien (`/var/log/kern.log`) reflektiert, da auch hier die Zeitstempel zunächst zurück und dann wieder vorspringen:

```
Mar 25 20:46:38 Schlepptop kernel: [99262.310544] usb 2-1: Product: USB Tablet
Mar 25 20:46:38 Schlepptop kernel: [99262.310546] usb 2-1: Manufacturer: VirtualBox
Mar 25 20:46:38 Schlepptop kernel: [99262.328111] input: VirtualBox USB Tablet as /devices/pci0000:00/0000:00:06.0/usb2/2-1/2-1:1.0/0003:80EE:0021.0002/input/input8
Mar 25 20:46:38 Schlepptop kernel: [99262.384832] hid-generic 0003:80EE:0021.0002: input,hidraw0: USB HID v1.10 Mouse [VirtualBox USB Tablet] on usb-0000:00:06.0-1/input0
Mar 24 19:13:52 Schlepptop kernel: [102775.652482] usb 2-1: USB disconnect, device number 3
Mar 24 19:13:52 Schlepptop kernel: [102776.010534] usb 2-1: new full-speed USB device number 4 using ohci-pci
Mar 24 19:13:52 Schlepptop kernel: [102776.268659] usb 2-1: New USB device found, idVendor=80ee, idProduct=0021
Mar 24 19:13:52 Schlepptop kernel: [102776.268664] usb 2-1: New USB device strings: Mfr=1, Product=3, SerialNumber=0
Mar 24 19:13:52 Schlepptop kernel: [102776.268665] usb 2-1: Product: USB Tablet
Mar 24 19:13:52 Schlepptop kernel: [102776.268667] usb 2-1: Manufacturer: VirtualBox
Mar 24 19:13:52 Schlepptop kernel: [102776.288165] input: VirtualBox USB Tablet as /devices/pci0000:00/0000:00:06.0/usb2/2-1/2-1:1.0/0003:80EE:0021.0003/input/input9
Mar 24 19:13:52 Schlepptop kernel: [102776.288299] hid-generic 0003:80EE:0021.0003: input,hidraw0: USB HID v1.10 Mouse [VirtualBox USB Tablet] on usb-0000:00:06.0-1/input0
Mar 25 21:47:41 Schlepptop kernel: [102917.085912] usb 2-1: USB disconnect, device number 4
Mar 25 21:47:42 Schlepptop kernel: [102917.442147] usb 2-1: new full-speed USB device number 5 using ohci-pci
```

Etwa eine Stunde später (um 19:15 "falsche Zeit") wird der NTP Dienst wieder gestartet, was wiederum den Systemlogs und dem root-Shell Eingabeverlauf zu entnehmen ist.

`/root/.bash_history`

```
vim /etc/default/ntp
vi /etc/default/ntp
ntp
ntpd
date 032418232017.00
date
```

Am 25. März 21:48 wird der Rechner dann ordnungsgemäß heruntergefahren, nur um wenige Minuten später (22:01) wieder gestartet zu werden. Zu diesem Zeitpunkt enden die Systemlogdateien.

Somit muss die Aussagekraft von Zeitstempeln in sämtlichen anderen Programmen, die in diesem Bericht analysiert werden (v.a. Firefox Browser und Skype aber auch Zeitstempel des Dateisystems), auch stark angezweifelt werden.

`/home/kurt/.bash_history`

```
history -w
history -w
exit
```

An dieser Stelle sei auch erwähnt, dass die `.bash_history` Datei des Nutzers `kurt` (welche automatisch alle Kommandozeilenbefehle des Nutzer abspeichert) fast leer und als letzten Befehl das Kommando `history -w` enthält, welches die aktuelle Befehle der Shell in die Datei schreibt. Dieses Verhalten ist sehr ungewöhnlich. Eine mögliche Erklärung ist, dass die `.bash_history` Datei zuvor durch den Nutzer gelöscht wurde, die vergangegen Befehle entfernt wurde (bzw. mit `history -c`) und daraufhin eine neuer, leerer Verlauf mit `history -w` geschrieben wurde.

Auffällig ist, dass am 25. März um 22:47 Skype von dem Rechner deinstalliert wurde, nachdem es am 24. März gegen 17:27 installiert wurde:

`/var/log/dpkg.log`

```
2017-03-24 17:27:54 install skype-bin:i386 <none> 4.3.0.37-0ubuntu0.12.04.1
2017-03-24 17:27:54 status half-installed skype-bin:i386 4.3.0.37-0ubuntu0.12.04.1
p2017-03-24 17:27:54 status triggers-pending gnome-menus:amd64 3.13.3-6ubuntu3.1
2017-03-24 17:27:54 status triggers-pending desktop-file-utils:amd64 0.22-1ubuntu5
2017-03-24 17:27:54 status half-installed skype-bin:i386 4.3.0.37-0ubuntu0.12.04.1
2017-03-24 17:27:54 status triggers-pending bamfdaemon:amd64 0.5.3~bzr0+16.04.20160701-0ubuntu1
2017-03-24 17:27:54 status triggers-pending mime-support:all 3.59ubuntu1
2017-03-24 17:27:55 status triggers-pending hicolor-icon-theme:all 0.15-0ubuntu1
2017-03-24 17:27:55 status half-installed skype-bin:i386 4.3.0.37-0ubuntu0.12.04.1
2017-03-24 17:27:57 status unpacked skype-bin:i386 4.3.0.37-0ubuntu0.12.04.1
2017-03-24 17:27:57 install skype:amd64 <none> 4.3.0.37-0ubuntu0.12.04.1
2017-03-24 17:27:57 status half-installed skype:amd64 4.3.0.37-0ubuntu0.12.04.1
2017-03-24 17:27:57 status unpacked skype:amd64 4.3.0.37-0ubuntu0.12.04.1
```

`/var/log/apt/history.log`

```
Start-Date: 2017-03-25  22:47:11
Commandline: apt-get remove skype
Requested-By: kurt (1000)
Remove: skype:amd64 (4.3.0.37-0ubuntu0.12.04.1)
End-Date: 2017-03-25  22:47:14
```

### Zimmermannhammer / Werkzeugkoffer der Marke Nagelflix

Auf dem gesamten Festplattenabbild konnten keine Hinweise darauf gefunden werden, dass Kurt K. einen Zimmermannhammer oder einen Werkzeugkoffer der Marke Nagelflix erworben hat. Hierzu wurde sowohl in der Hautppartition als auch in den restlichen Partitionen nach den Ausdrücken `nagelflix`, `hammer`, `koffer` und `werkzeug` gesucht (`grep -ri 'nagelflix\|hammer\|koffer\|werkzeug' /`). Neben der Hauptpartition des Systems (part2) wurde insbesondere auch die Swap-Partitionen part4 sowie part6 untersucht.

### Browserverlauf

Das Browserprofil wurde erst am 23. März 2017 erstellt, somit sind keine Aussagen über den Webseitenverlauf vorher möglich. Bei der Analyse wurde die Datei `/home/kurt/.mozilla/firefox/21fa3lqk.default/places.sqlite` untersucht. Die besuchten Webseiten sind im Abschnitt Anhang/Browserverlauf zu sehen.

Daraus ist ersichtlich, dass der Benuzter am 24. März 2017 zum ersten Mal den Firefox Browser in Version 47 gestartet hat (`/firefox/47.0/firstrun`).
Danach hat er recherchiert, wie man Skype auf Ubuntu Linux installiert (16:19 Uhr).
Gegen 16:30 deselben Tages hat er dann Gmail geöffnet, vermutlich um die E-Mail an Veronika V. zu schreiben (siehe [Abschnitt Zugriff auf E-Mail-Postfach](#zugriff-auf-e-mail-postfach)).
Kurz nach 17 Uhr hat sich der Benutzer mehrere Male ein Video auf der Video-Platform YouTube angesehen.

Am 25. März um kurz vor 19 Uhr hat sich der Benutzer bei Skype registriert.

### Skype Nutzung

Es wird das Skype Profil unter `/home/kurt/.Skype/live#3akunz.kurt9` untersucht. Auffällig ist hierbei, dass alle Dateien zuletzt am 25. März bearbeitet wurden, außer `config.xml`, `main.db` sowie `main.db-journal`, welche zuletzt am Vortag (24. März) modifiert wurden. Da die Datei `main.db` die zentrale Datenbank des Skype-Programms ist, scheint es unwahrscheinlich, dass diese bei einem Programmstart nicht modifiert wird.


Chatverlauf zwischen den Skype-Nutzern `kunz.kurt9` und `vogelvroni`:

```sql
select id,author,datetime(timestamp, 'unixepoch', 'localtime'),body_xml from Messages;
```

ID  | Autor         | Zeitstempel       | Nachricht
----|---------------|-------------------|-------------------------------
74|live: kunz.kurt9|2017-03-25 20:20:31|Hallo Vroni Vogel! Hab deinen account gefunden.
78|live: vogelvroni|2017-03-25 20:20:49|
79|live: vogelvroni|2017-03-25 20:21:01|Hallo Kurt!
80|live: kunz.kurt9|2017-03-25 20:21:18|Hallo Vroni!
81|live: vogelvroni|2017-03-25 20:21:52|Bin in der Küche.
82|live: kunz.kurt9|2017-03-25 20:22:06|Und ich im Arbeitszimmer.
83|live: vogelvroni|2017-03-25 20:22:16|Essen fertig.
84|live: kunz.kurt9|2017-03-25 20:22:22|Bin aufm Weg
89|live: kunz.kurt9|2017-03-24 18:23:20|*(XML Body)*

```
<partlist alt="">
  <part identity="live:kunz.kurt9">
    <name>Kurt Kunz</name>
    <duration>2995</duration>
  </part>
  <part identity="live:vogelvroni">
    <name>Vroni Vogel</name>
    <duration>2995</duration>
  </part>
</partlist>
90|live:kunz.kurt9|2017-03-24 19:13:15|<partlist alt="">
  <part identity="live:kunz.kurt9">
    <name>Kurt Kunz</name>
    <duration>2995</duration>
  </part>
  <part identity="live:vogelvroni">
    <name>Vroni Vogel</name>
    <duration>2995</duration>
  </part>
</partlist>
```

Der Nachrichtenverlauf zeigt, dass Kurt K. und Veronika V. sich am 25. März 2017 um 20:20 Uhr geschrieben haben. Dies wird auch durch den Text der Nachricht bestätigt. Bei der Nachricht mit ID 89 ändert sich jedoch der Zeitstempel auf den 24. März 2017 um 18:23 Uhr. Der Primärschlüssel der SQL-Tabelle (ID) wird jedoch weiterhin inkrementiert, somit scheint eine Manipulation der Rechneruhr wahrscheinlich. Zu dieser Zeit hat ein Skype-Gespräch von 49 Minuten zwischen Kurt K. und Veronika V. statt gefunden, was auch durch den Eintrag in der Tabelle `Calls` bestätigt wird.

```sql
select id,name,datetime(begin_timestamp, 'unixepoch', 'localtime'),duration/60 from Calls;
```
ID | Name | Zeitstempel | Dauer
---|------|-------------|------
85|1-1490376194|2017-03-24 18:23:14|49

Es ist jedoch fraglich, ob der Zeitstempel korrekt ist.


### Zugriff auf E-Mail-Postfach

Der Zugriff auf das E-Mail-Postfach von Kurt K. ist nicht möglich, da kein lokaler E-Mail-Client (gesucht wurde nach mutt, Thunderbird, Evolution und K-Mail) verwendet wird. Zwar sind Thunderbird und Evolution auf dem Rechner installiert, jedoch werden diese nicht verwendet (keine entsprechende Ordner unter `/home/kurt/`). Somit sind auf dem Rechner keine E-Mails gespeichert.

Es konnte jedoch eine E-Mail von Kurt K., welche dieser im Webbrowser mit Gmail an Veronika V. geschrieben hat, aus dem Swap (Partition 6) wiederhergestellt werden.

Klartext (Original siehe [Anhang/E-Mail an Veronika V.](#e-mail-en-veronika-v.)):

> Hallo Schatzi, ich werde den Koffer gleich ins Auto stellen, damit ich ihn morgen nicht vergesse.

> 2017-03-24 17:50 GMT+01:00 Veronika Vogel:
>
> Hey Schatzi, sch.n,dass du es geschafft hast, .skype zu installieren auf deinem neuen Laptop.
>
> Das k.nnen wir ja direkt .mal ausprobieren. Aber du musst mir am Wochenende erkl.ren, wie man das .auf dem Handyinstalliert.
>
> Oh und vergiss nicht, .morgen den Werkzeugkoffer mitzunehmen, dann k.nnen wir den Gartenzaun .reparieren, ich hab schon gelbe Farbe und Pinsel besorgt.

Veronika Vogel hat ihre Nachricht mit an Sicherheit-grenzender Wahrscheinlichkeit am 24. März um 17:50 geschrieben, da dieser Zeitstempel von Googles Server kommt. Wann Kurt K. jedoch seine Antwort verfasst beziehungsweise geschrieben hat, lässt nicht klären.

Auf den restlichen Festplattenabschnitten (insbesondere part2 und part4) wurden keine E-Mail Daten gefunden.

### Fotos und Überwachung des Tatopfers

Auf der Hautpsystempartition (part2) wurden keine Fotos einer Digitalkamera (`jpg`, `jpeg`), fallrelevante Textdokumente (`odt`, `doc`, `docx`, `pdf`) oder Tabellendokumete (`ods`, `csv`, `xls`, `xlsx`) gefunden (mit `find / -iname *.EXT`). Sämtliche private Verzeichnisse des Nutzers `kurt` sind leer (Dokumente, Downloads, Musik, Bilder, Videos).

Selbiges gilt für die Festplattenabschnite part0 (Boot Sektor), part1, part3 (Unallozierter Bereich), part4 (ehemals Swap-Partition), part5 (Unallozierter Bereiche), und part6 (aktueller Swap-Bereich), die mit dem Filecarver PhotoRec untersucht wurden.

### Wiederherstellung gelöschter Dokumente

Es finden sich keine nur augenscheinlich gelöschten Dokumente, das heißt sie wurden vom Nutzer nur in den Papierkorb verschoben, auf dem System gefunden (`$HOME/Trash` bzw. Gnome-spezifischer Pfad `$HOME/.local/share/Trash`)

Wie der fsstat Ausgabe zu entnehmen ist, finden sich in dem Abbild 12 gelöschte Dateien:

```
Orphan Inodes: 8902, 8901, 8239, 8238, 8237,
               393487, 393479, 393484, 393481,
               393485, 393483, 393405,
```

Eine Wiederherstellung war mit dem icat Tool aus der Sleuth Kit Sammlung jedoch nicht möglich. Eine plausible Erklärung hierfür ist, dass in dem verwendeten Laptop (Dell XPS 13) eine SSD verbaut ist. Bei diesem Typ von Datenträger sendet das Betriebssystem sogenannte TRIM-Befehle an den Datenträger, woraufhin dieser die Blöcke tatsächlich löscht und danach womöglich wiederverwendet. Deswegen ist eine Rekonstruktion dieser Dateien nicht mehr möglich.


### Abschluss

Abschließend werden die Prüfsummen der einzelenen Partitionen sowie des gesamten Festplattenabbildes erneut überprüft.

```
2d087c1c86ad6d3589b26d315d8dd7956dd5dbb27dc880418ef05c40b845f5b0  exercise_2.img
8d76d04401a40f648c1253a29234819f465a6556436c7c8fae0b5486eb9b6b6c  part0
bcd62afdbad5ff3b617a3d14b48baf196beaa8022c73f8a2164a68374ee9c31e  part1
f8b72ae4b289f9e5361028f2f31e2ff348a074178f9994f27265a3c78efaff56  part2
9aa25cc54fb837a197b2b3f602f43c1dffb7193f82522fb1b4aadaadb8d79f62  part3
45b3397d59a7618bd543434d9c9b7e92b52e80e7314b098072b61acd4071fbac  part4
57990bf28dacc4dc5ad367b090c487100e50ce715ee872ce3785cdd8c050ed60  part5
28108cf0c95c895e0aa3916b1bd1ae94ccb550764899a6518915144690fa3a3e  part6
30e14955ebf1352266dc2ff8067e68104607e750abb9d3b36582b8af909fcb58  part7
```

Diese stimmen nach wie vor mit den ursprünglichen Prüfsummen überein, es sind also während der Untersuchungen an den Daten keine versehentlichen oder absichtlichen Veränderungen aufgetreten.

\newpage

## Anhang

### E-Mail an Veronika V.

```
<div dir="ltr"><div><div><div>
Hallo Schatzi,<br><br></div>ich werde den Koffer gleich ins Auto stellen, damit ich ihn morgen nicht vergesse.
<br><br></div>
Bussi,<br><br></div>Kurti
<br></div><div class="HOEnZb"><div class="adm"><div id="q_15b015183e534604_1" class="ajR h4"><div class="ajT"></div></div></div><div class="h5">
<div class="gmail_extra"><br><div class="gmail_quote">2017-03-24 17:50 GMT+01:00 Veronika Vogel <span dir="ltr">&lt;<a href="mailto:vogelvroni@gmail.com" target="_blank">vogelvroni@gmail.com</a>&gt;</span>:<br><blockquote class="gmail_quote" style="margin:0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex"><div dir="ltr"><div><div>
Hey Schatzi,<br><br></div>sch.n,dass du es geschafft hast, .skype zu installieren auf deinem neuen Laptop. Dask.nnen wir ja direkt .mal ausprobieren. Aber du musst mir am Wochenende erkl.ren, wie man das .auf dem Handyinstalliert.<br><br></div><div>Oh und vergiss nicht, .morgen den Werkzeugkoffer mitzunehmen, dann k.nnenwir den Gartenzaun .reparieren, ich hab schon gelbe Farbe und Pinsel besorgt.
<br><br></div><div>Bussi,<br><br></div>Vroni</div>.</blockquote></div><br></div>.</div></div>
```

### Browserverlauf

`/home/kurt/.mozilla/firefox/21fa3lqk.default/places.sqlite`

```sql
SELECT datetime(moz_historyvisits.visit_date/1000000,'unixepoch'), moz_places.url
FROM moz_places, moz_historyvisits
WHERE moz_places.id = moz_historyvisits.place_id;
```

Datum                   | URL
------------------------|--------------------------------------------------------------------
2017-03-24 16:18:03|\url{https://www.mozilla.org/en-US/firefox/47.0/firstrun/learnmore/}
2017-03-24 16:18:04|\url{https://www.mozilla.org/en-US/firefox/features/}
2017-03-24 16:19:05|https://www.google.com/search?client=ubuntu&channel=fs &q=skype&ie=utf-8&oe=utf-8
2017-03-24 16:19:06|https://www.google.de/search?client=ubuntu&channel=fs &q=skype&ie=utf-8&oe=utf-8&gfe_rd=cr&ei=-UbVWIeIO9Pi8Af21bu4Ag
2017-03-24 16:19:12|https://www.skype.com/de/download-skype/skype-for-linux/
2017-03-24 16:19:31|https://www.google.com/search?client=ubuntu&channel=fs &q=skype+for+ubuntu+repo&ie=utf-8&oe=utf-8
2017-03-24 16:19:31|\url{https://www.google.de/search?client=ubuntu&channel=fs &q=skype+for+ubuntu+repo&ie=utf-8&oe=utf-8&gfe_rd=cr&ei=E0fVWMJB0-LwB_bVu7gC}
2017-03-24 16:19:38|\url{https://wiki.ubuntuusers.de/Skype/}
2017-03-24 16:19:45|\url{https://wiki.ubuntuusers.de/Xenial_Xerus/}
2017-03-24 16:29:22|\url{http://gmail.com/}
2017-03-24 16:29:22|\url{https://www.google.com/gmail/}
2017-03-24 16:29:23|\url{https://mail.google.com/mail/}
2017-03-24 16:30:06|\url{https://mail.google.com/mail/}
2017-03-24 16:30:53|\url{https://mail.google.com/mail/}
2017-03-24 16:29:23|\url{https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=googlemail&emr=1&osid=1}
2017-03-24 16:30:22|\url{https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=googlemail&emr=1&osid=1}
2017-03-24 16:29:23|\url{https://mail.google.com/intl/en/mail/help/about.html}
2017-03-24 16:29:23|\url{https://www.google.com/intl/en/mail/help/about.html}
2017-03-24 16:29:23|\url{https://www.google.com/gmail/about/}
2017-03-24 16:29:36|\url{https://accounts.google.com/AccountChooser?service=mail&continue=https://mail.google.com/mail/}
2017-03-24 16:29:36|\url{https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1}
2017-03-24 16:29:36|\url{https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1#identifier}
2017-03-24 16:29:53|\url{https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1#password}
2017-03-24 16:29:59|\url{https://accounts.google.com/CheckCookie?checkedDomains=youtube&checkConnection=youtube%3A658%3A1&pstMsg=1&chtml=LoginDoneHtml&service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&gidl=...}
2017-03-24 16:29:59|\url{https://myaccount.google.com/interstitials/recoveryoptions?authuser=0&hl=en&service=mail&continue=...}
2017-03-24 16:29:59|\url{https://accounts.google.com/ServiceLogin?service=accountsettings&passive=1209600&osid=1...}
2017-03-24 16:29:59|\url{https://myaccount.google.com/accounts/SetOSID?continue=...}
2017-03-24 16:29:59|\url{https://myaccount.google.com/interstitials/recoveryoptions?hl=en&service=mail...}
2017-03-24 16:30:05|\url{https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F...}
2017-03-24 16:30:05|\url{https://mail.google.com/accounts/SetOSID?authuser=0&continue=https%3A%2F%2Faccounts.youtube.com...}
2017-03-24 16:30:05|\url{https://accounts.youtube.com/accounts/SetSID?ssdc=1&sidt=...}
2017-03-24 16:30:05|\url{https://accounts.google.de/accounts/SetSID?ssdc=1&sidt=...}
2017-03-24 16:30:05|\url{https://mail.google.com/mail/?pli=1}
2017-03-24 16:30:08|\url{https://mail.google.com/mail/#inbox}
2017-03-24 16:30:55|\url{https://mail.google.com/mail/#inbox}
2017-03-24 16:48:05|\url{https://mail.google.com/mail/#inbox}
2017-03-24 16:48:25|\url{https://mail.google.com/mail/#inbox}
2017-03-24 16:30:20|\url{https://accounts.google.com/Logout?hl=en-GB&continue=https://mail.google.com/mail...}
2017-03-24 16:30:20|\url{https://accounts.youtube.com/accounts/Logout2?hl=en-GB&service=mail&ilo=1&ils=s.youtube%2Cdoritos...}
2017-03-24 16:30:21|\url{http://www.google.com/accounts/Logout2?hl=en-GB&service=mail&ilo=1&ils=doritos...}
2017-03-24 16:30:21|\url{http://www.google.de/accounts/Logout2?hl=en-GB&service=mail&ilo=1&ils=...}
2017-03-24 16:30:21|\url{https://mail.google.com/mail}
2017-03-24 16:30:53|\url{https://mail.google.com/accounts/SetOSID?authuser=0&continue=...}
2017-03-24 16:48:04|\url{https://mail.google.com/mail/#starred}
2017-03-24 16:48:23|\url{https://mail.google.com/mail/#starred}
2017-03-24 16:48:24|\url{https://mail.google.com/mail/#sent}
2017-03-24 16:50:22|\url{https://mail.google.com/mail/#inbox/15b0139c21c9dac6}
2017-03-24 17:11:07|\url{https://www.google.com/search?client=ubuntu&channel=fs&q=youtube+country+musik&ie=utf-8&oe=utf-8}
2017-03-24 17:11:07|\url{https://www.google.de/search?client=ubuntu&channel=fs&q=youtube+country+musik&ie=utf-8&oe=utf-8&gfe_rd=cr&ei=K1PVWLGtHNDi8Afx37_wCA}
2017-03-24 17:11:13|\url{https://www.youtube.com/watch?v=OOcH1oHz68c}
2017-03-24 17:28:01|\url{https://www.youtube.com/watch?v=OOcH1oHz68c}
2017-03-24 17:28:59|\url{https://www.youtube.com/watch?v=OOcH1oHz68c}
2017-03-24 17:22:23|\url{http://youtube.com/html5}
2017-03-24 17:22:23|\url{http://www.youtube.com/html5}
2017-03-24 17:22:23|\url{https://www.youtube.com/html5}
2017-03-25 18:54:38|\url{http://www.skype.com/go/registration?setlang=de&intsrc=client%7Creg-a%7C2/4.3.0.37/172}
2017-03-25 18:54:38|\url{http://www.skype.com/go/registration?setlang=de&intsrc=client%7Creg-a%7C2/4.3.0.37/172}
2017-03-25 19:01:37|\url{http://www.skype.com/go/registration?setlang=de&intsrc=client%7Creg-a%7C2/4.3.0.37/172}
